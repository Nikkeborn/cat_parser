import requests
import json
import csv


payload = {'type': None, 'text': None}
r = requests.get('https://cat-fact.herokuapp.com/facts/', params = payload)


if r.status_code == requests.codes.ok:
    print(f'request status code: {r.status_code} Ok ')


    answer = r.json()
    if len(answer) != 0:
    

        ans_list: list = answer['all']


        with open('/home/wis/Desktop/answer.json', 'w') as fa:
            json.dump(ans_list, fa, indent='	')
            

        with open('/home/wis/Desktop/answer.json', 'r') as faj:    
            json_list: list = json.load(faj)


        def ElToList(listok: list) -> list:
            text_list = []
            j =0
            while len(listok) != j:
                text_list.append(listok[j]['text'])
                j +=1
            return text_list


        data_list = ElToList(json_list)


        def csv_writer(data, path):
            """
            Write data to a CSV file path
            """
            with open(path, "w", newline='\r\n') as csv_file:
                writer = csv.writer(csv_file, delimiter=',')
                
                header = ['number','fact']
                writer.writerow(header)
                
                for num, text in enumerate(data):
                    writer.writerow((num + 1, text))


        csv_writer(data_list, '/home/wis/Desktop/cat_facts.csv')

    
    else:
        print('an empty server response was received')


else:
    print('no response from the server')



if __name__=='__main__':
    pass


#########################################debug##########################################
# print('\n################################debug##########################################')
# print(r.url)
# print(r.status_code)
# # print(answer)
# # print(ans_list)
# print(type(ans_list))
# print(len(ans_list))
# # print(json_list)
# print(len(json_list))
# print(type(json_list))
# # print(data_list)
# print(type(data_list))
# structure description json_arr[['_id':'','text':'','type':['_id':'','name':['first':'','last':'']]].......]
# , newline='\r\n'
# , delimiter=','
#########################################debug##########################################
# print('\n################################debug##########################################')